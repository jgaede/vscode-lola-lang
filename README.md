# lola-lang README

This extension provides basic syntax highlighting for LoLA files, without grammar checking and anything.

More features like syntax checking or snippets may or may not be added at a later date.